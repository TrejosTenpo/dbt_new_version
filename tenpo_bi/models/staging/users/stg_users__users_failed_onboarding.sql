with

source as (

    select * from {{ source('tenpo_users', 'users_failed_onboarding') }}  

),

renamed as (

    select
        user_id id, 
        {{ hash_sensible_data('email') }} as email, 
        '' as tributary_identifier, 
        {{ hash_sensible_data('phone') }} as phone,
        state, 
        level,
        plan,
        region_code, 
        profession, 
        nationality, 
        country_code,
        agree_terms_conditions, 
        is_card_active, 
        created_at,
        date(created_at) as fecha_creacion,
        updated_at, 
        {{ hash_sensible_data('first_name') }} as first_name

    from source 
)

select * from renamed
qualify
    row_number() over (partition by email order by created_at desc) = 1 