with

source as (

    select * from {{ source('tenpo_users', 'users') }}  

),

renamed as (

    select
        id, 
        {{ hash_sensible_data('email') }} as email, 
        tributary_identifier, 
        {{ hash_sensible_data('phone') }} as phone, 
        state, 
        level, 
        plan,
        region_code, 
        profession, 
        nationality, 
        country_code,
        agree_terms_conditions, 
        is_card_active, 
        is_tef_validated,  
        created_at,
        date(created_at) as fecha_creacion,
        updated_at, 
        date_of_birth,
        ob_completed_at,
        DATETIME(ob_completed_at, "America/Santiago") as ob_completed_at_dt_chile,
        {{ hash_sensible_data('first_name') }} as first_name,
        source,
        address, 
        gender,
        validated,
        marital_status,
        category,
        region,
        commune,
        has_credit_card

    from source 
)

select * from renamed
qualify
    row_number() over (partition by CAST(id AS string) order by updated_at desc) = 1 