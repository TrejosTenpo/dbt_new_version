
{{ 
  config(
    materialized='view',
  ) 
}}


with

source as (

    select * from {{ source('prepago', 'prp_cuenta') }} 

),

renamed as (

    select
      id,
      uuid,
      uuid as account_id,
      id_usuario,
      cuenta,
      procesador,
      saldo_info,
      saldo_expiracion,
      estado,
      creacion,
      actualizacion,
      account_reconciled,
      nivel,
      tipo,
      tipo_validacion

    from source 
)

select * from renamed
qualify
    row_number() over (partition by CAST(account_id AS string) order by actualizacion desc) = 1 


