{{ 
  config(
    materialized='view',
  ) 
}}


with

source as (

    select * from {{source('prepago','tipos_factura')}} 

),

renamed as (

    select
        tipofac,
        indnorcor,
        signo,
        glosa,
        tipo

    from source 
)

select * from renamed
