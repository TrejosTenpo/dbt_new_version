{{ 
  config(
    materialized='view',
  ) 
}}


with

source as (

    select * from {{ source('prepago', 'prp_movimiento') }} 

),

renamed as (

    select
        tipofac,
        tipo_movimiento,
        nummovext,
        numextcta,
        numaut,
        tipolin,
        nompob,
        nomcomred,
        id_tx_externo,
        monto as scs_monto,
        linref,
        impdiv,
        id_tarjeta,
        codact,
        id_movimiento_ref,
        codcom,
        CAST(codcom as STRING) as id_comercio,
        id,
        fecha_creacion,
        DATE(fecha_creacion  , "America/Santiago") AS fecha,
        fecha_creacion as trx_timestamp,
        fecha_actualizacion,
        indnorcor,
        fecfac,
        estado_de_negocio,
        impfac,
        impfac as monto,
        cmbapli,
        id_usuario,
        uuid,
        uuid as trx_id,
        impliq,
        estado_con_switch,
        estado,
        estado_con_tecnocom,
        centalta,
        numbencta,
        clamone,
        clamon,
        codent,
        cuenta,
        clamonliq,
        origen_movimiento,
        numreffac,
        codpais,
        clamondiv,
        third_party_external_id,
        actividad_cd,
        actividad_nac_cd,
        es_comercio_presencial,
        multiclearing_type

    from source 
)

select * from renamed
qualify
    row_number() over (partition by CAST(trx_id AS string) order by fecha_actualizacion desc) = 1 