{{ 
  config(
    materialized='view',
  ) 
}}


with

source as (

    select * from {{ source('prepago', 'prp_tarjeta') }} 

),

renamed as (

    select
        id,
        uuid,
        uuid as id_tarjeta,
        estado,
        producto,
        numero_unico,
        id_cuenta,
        numplastico,
        fecha_creacion,
        fecha_actualizacion,
        tipo,
        red

    from source 
)

select * from renamed
qualify
    row_number() over (partition by CAST(id_tarjeta AS string) order by fecha_actualizacion desc) = 1 