{{ 
  config(
    materialized='view',
  ) 
}}


with

source as (

    select * from {{ source('prepago', 'prp_usuario') }} 

),

renamed as (

    select   
        id,
        uuid,
        uuid as user,
        estado,
        nivel,
        tipo_documento,
        plan,
        fecha_creacion,
        fecha_actualizacion    

    from source 
)

select * from renamed
qualify
    row_number() over (partition by CAST(user AS string) order by fecha_actualizacion desc) = 1 
