{{ 
  config(
    materialized='view',
  ) 
}}


with

source as (

    select * from {{source('aux','codigo_pais')}}

),

renamed as (

    select
      pais,
      ISO_3166_1_alpha3,
      ISO_3166_1_num

    from source 
)

select * from renamed