{{ 
  config(
    materialized='view',
  ) 
}}


with

source as (

    select * from {{source('aux_table','mcc_codes')}}

),

renamed as (

    select
      MCC,
      cat_tenpo1,
      cat_tenpo2,
      cat_tenpo3

    from source 
)

select * from renamed