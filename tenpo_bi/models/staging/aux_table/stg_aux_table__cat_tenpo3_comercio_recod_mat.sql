{{ 
  config(
    materialized='view',
  ) 
}}


with

source as (

    select * from {{source('aux_table','cat_tenpo3_comercio_recod_mat')}}

),

renamed as (

    select
      comercio_recod,
      cat_tenpo3

    from source 
)

select * from renamed