{{ 
  config(
    materialized='view',
  ) 
}}


with

source as (

    select * from {{source('aux','cof_comercios')}}

),

renamed as (

    select
      comercio_recod,
      id_comercio

    from source 
)

select * from renamed