{{ 
  config(
    materialized='view',
  ) 
}}


with

source as (

    select * from {{source('aux_table','cat_tenpo3_commerce_mat')}}

),

renamed as (

    select
      nombre_comercio,
      category_new

    from source 
)

select * from renamed