{{ 
  config(
    materialized='view',
  ) 
}}


with

source as (

    select * from {{source('aux_table','mcc_master_codes')}} 

),

renamed as (

    select
      mcc,
      edited_description,
      combined_description,
      usda_description,
      irs_description,
      irs_reportable

    from source 
)

select * from renamed