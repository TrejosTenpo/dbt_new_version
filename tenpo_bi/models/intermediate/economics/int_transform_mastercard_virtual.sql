{{ 
  config(
    materialized='ephemeral'
  ) 
}}


SELECT 
    * EXCEPT (es_comercio_presencial)
    ,'mastercard' as linea
FROM {{ ref('int_transform_mastercard') }}
WHERE (es_comercio_presencial is false OR es_comercio_presencial is null )
