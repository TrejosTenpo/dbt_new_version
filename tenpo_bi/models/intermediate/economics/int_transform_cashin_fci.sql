
{{ 
  config(
    materialized='ephemeral'
  ) 
}}

SELECT
    user,
    fecha,
    trx_timestamp,
    monto,
    trx_id
FROM {{ ref('int_transform_cashin') }}
QUALIFY 
    row_number() over (partition by user order by trx_timestamp asc) = 1
