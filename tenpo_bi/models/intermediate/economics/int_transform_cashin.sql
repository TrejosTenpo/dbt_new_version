
{{ 
  config(
    materialized='ephemeral'
  ) 
}}


SELECT 
    DATE(m.fecha_creacion  , "America/Santiago") AS fecha,
    m.fecha_creacion as trx_timestamp,
    CASE WHEN m.tipofac = 3002 THEN 'Cashin físico Klap' 
            WHEN m.tipofac = 3016 THEN 'Cashin moneysend' 
            WHEN m.tipofac = 711 THEN 'Cashin físico Unired' 
            WHEN m.tipofac = 90 THEN 'Cashin físico Sencillito' 
            WHEN m.tipofac = 3053 THEN 'Cashin Pago Nomina'
            WHEN m.tipofac = 3057 THEN 'Cashback Tenpesos'
            WHEN m.tipofac = 720 THEN 'Cashin Transbank'
            WHEN fin.id IS NOT NULL THEN 'Cashin Fintoc'
            ELSE 'Cashin tef'
            END AS nombre,
    impfac  as monto,
    m.uuid as trx_id,
    u.uuid as user,
    c.uuid as account_id,  
    'cash_in' linea,
    'app' canal,
    'n/a' as comercio,
    CAST(m.codcom as STRING) as id_comercio,
    actividad_cd,
    actividad_nac_cd,
    m.codact,
    m.tipofac,
    t.uuid as id_tarjeta,
    CASE
        WHEN m.tipofac IN (3002,711) AND uf.rate IS NULL AND uf_h.UF IS NULL THEN (rev.tasa_final*35000)*-1 
        WHEN m.tipofac IN (3002,711) AND DATE(m.fecha_creacion, "America/Santiago") <= '2022-10-31' THEN (rev.tasa_final*uf_h.UF)*-1
        WHEN m.tipofac IN (3002,711) AND DATE(m.fecha_creacion, "America/Santiago") > '2022-10-31' THEN (rev.tasa_final*uf.rate)*-1     
        WHEN m.tipofac = 3001 AND uf.rate IS NULL AND uf_h.UF IS NULL THEN rev_tef.tasa_final*35000                     
        WHEN m.tipofac = 3001 AND DATE(m.fecha_creacion, "America/Santiago") <= '2022-10-31' THEN rev_tef.tasa_final*uf_h.UF
        WHEN m.tipofac = 3001 AND DATE(m.fecha_creacion, "America/Santiago") > '2022-10-31' THEN rev_tef.tasa_final*uf.rate   
        WHEN m.tipofac = 720 THEN impfac*0.0225                  
        ELSE 0
    END as revenue
FROM {{ source('prepago', 'prp_cuenta') }} c
    JOIN {{ source('prepago', 'prp_usuario') }} u ON c.id_usuario = u.id
    JOIN {{ source('prepago', 'prp_tarjeta') }} t ON c.id = t.id_cuenta
    JOIN {{ source('prepago', 'prp_movimiento') }} m ON m.id_tarjeta = t.id
    LEFT JOIN {{ source('payment_cca', 'payment_transaction') }} p ON m.id_tx_externo = p.id
    LEFT JOIN {{ source('bancarization', 'fintoc_intent') }} fin ON u.uuid = fin.user AND DATE(m.fecha_creacion  , "America/Santiago") = fin.fecha AND m.impfac = fin.amount AND fin.status="succeeded" AND m.tipofac = 3001 
    LEFT JOIN (SELECT DISTINCT rate,verified_date FROM {{ source('exchange_rate', 'unit_rate') }} WHERE code = 'UF') uf ON DATE(m.fecha_creacion, "America/Santiago") = DATE(uf.verified_date, "America/Santiago") AND DATE(m.fecha_creacion, "America/Santiago") > '2022-10-31'
    LEFT JOIN {{source('aux','UF_historico')}} uf_h ON DATE(m.fecha_creacion, "America/Santiago") BETWEEN uf_h.fecha_inicio AND uf_h.fecha_fin AND DATE(m.fecha_creacion, "America/Santiago") <= '2022-10-31'    
    LEFT JOIN {{source('aux','cash_in_revenue')}} rev ON m.tipofac = rev.tipofac AND DATE(m.fecha_creacion, "America/Santiago") BETWEEN rev.fecha_inicio AND rev.fecha_fin 
    LEFT JOIN {{source('aux','cca_revenue')}} rev_tef ON m.tipofac = rev_tef.tipofac AND DATE(m.fecha_creacion, "America/Santiago") BETWEEN rev_tef.fecha_inicio AND rev_tef.fecha_fin AND CAST(CASE WHEN CAST(p.originSbifCode AS INT64) IN (12,730) THEN p.originSbifCode ELSE '0' END AS INT64) = rev_tef.SbifCode    
WHERE 
    m.estado    = 'PROCESS_OK'
    AND indnorcor  = 0
    AND m.tipofac in (3001,3002,3016,711,90,3053,3057,720)
    AND estado_de_negocio in ('OK','CONFIRMED')
    AND {{ dates_between('DATE(m.fecha_creacion  , "America/Santiago")') }} -- \Modelos_de_Datos\200_DBT_Tenpo_BI\dbt_tenpo_bi\macros\dates_between.sql 
QUALIFY 
    row_number() over (partition by CAST(m.uuid AS string) order by m.fecha_actualizacion desc) = 1