  {{ 
  config(
    materialized='ephemeral',
    enabled=True
  ) 
}}

select user_id, limit_card 
from  {{source('credit_card','user_product')}}
qualify row_number() over(partition by user_id order by updated_at desc) = 1