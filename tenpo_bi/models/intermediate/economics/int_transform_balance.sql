
{{ 
  config(
    materialized='ephemeral'
  ) 
}}

SELECT DISTINCT
    fecha 
    ,timestamp(fecha) as trx_timestamp
    ,'Saldo APP' as nombre
    ,saldo_dia as monto
    ,concat(user,"-",fecha) as trx_id
    ,user
    ,'saldo' as linea
    ,'app' as canal
    ,'n/a' as comercio
    ,CAST(null as STRING) as id_comercio
    ,CAST(null as STRING) as actividad_cd
    ,CAST(null as STRING) as actividad_nac_cd
    ,CAST(null AS FLOAT64) as codact 
    ,null as tipofac 
FROM {{ref('daily_balance')}} trx
where true
    and saldo_dia>0
qualify row_number() over (partition by user,fecha order by fecha)=1
