{{ 
  config(
    materialized='ephemeral'
  ) 
}}

SELECT
    DATE(m.fecha_creacion  , "America/Santiago") AS fecha,
    m.fecha_creacion as trx_timestamp,
    CASE 
        WHEN tipofac = 3006 THEN 'Suscrip. dist. peso'
        WHEN tipofac = 3007 THEN 'Compra dist. peso'
        WHEN tipofac = 3010 THEN 'Devolución compra'
        WHEN tipofac = 3011 THEN 'Devolución compra com. relacionado'
        WHEN tipofac = 3012 THEN 'Devolución compra nacional'
        WHEN tipofac = 3028 THEN 'Compra peso'
        WHEN tipofac = 3029 THEN 'Suscrip. peso'
        WHEN tipofac = 3030 THEN 'Devolución compra peso'
        WHEN tipofac in ( 3009, 3031, 5) THEN 'Nacional'
        END AS nombre,
    CASE 
        WHEN tipofac in (3011,3012,3030) THEN m.impfac*-1 
        WHEN tipofac = 3010 THEN m.impfac *-1.024
        WHEN tipofac in (3006, 3007) THEN m.impfac *1.024
        ELSE m.impfac END as monto,
    m.uuid as trx_id,
    u.uuid as user,
    c.uuid as account_id,    
    'app' as canal,
    nomcomred as comercio,
    CAST(m.codcom as STRING) as id_comercio,
    actividad_cd,
    actividad_nac_cd,
    m.codact,
    m.codpais,
    tipofac,
    es_comercio_presencial,
    t.uuid as id_tarjeta,
    CASE 
        WHEN tipofac in (3011,3012,3030) AND (es_comercio_presencial is false OR es_comercio_presencial is null ) THEN (m.impfac*-1)*mastercard.tasa 
        WHEN tipofac = 3010 AND (es_comercio_presencial is false OR es_comercio_presencial is null ) THEN (m.impfac *-1.024)*mastercard.tasa
        WHEN tipofac in (3006, 3007) AND (es_comercio_presencial is false OR es_comercio_presencial is null ) THEN (m.impfac *1.024)*mastercard.tasa
        WHEN tipofac in ( 3009, 3031, 5, 3028 ,3029) AND (es_comercio_presencial is false OR es_comercio_presencial is null ) THEN m.impfac*mastercard.tasa
        WHEN tipofac in (3011,3012,3030) AND es_comercio_presencial is true THEN (m.impfac*-1)*mastercard_phys.tasa 
        WHEN tipofac = 3010 AND es_comercio_presencial is true THEN (m.impfac *-1.024)*mastercard_phys.tasa
        WHEN tipofac in (3006, 3007) AND es_comercio_presencial is true THEN (m.impfac *1.024)*mastercard_phys.tasa
        WHEN tipofac in ( 3009, 3031, 5, 3028 ,3029) AND es_comercio_presencial is true THEN m.impfac*mastercard_phys.tasa
        ELSE 0 END as revenue
FROM {{ ref('stg_prepago__prp_cuenta') }} c
    JOIN {{ ref('stg_prepago__prp_usuario') }} u ON c.id_usuario = u.id
    JOIN {{ ref('stg_prepago__prp_tarjeta') }} t ON c.id = t.id_cuenta
    JOIN {{ ref('stg_prepago__prp_movimiento') }} m ON m.id_tarjeta = t.id
    LEFT JOIN {{ source('aux', 'mastercard_revenue') }} mastercard on
    CASE 
        WHEN tipofac = 3006 THEN 'Suscrip. dist. peso'
        WHEN tipofac = 3007 THEN 'Compra dist. peso'
        WHEN tipofac = 3010 THEN 'Devolución compra'
        WHEN tipofac = 3011 THEN 'Devolución compra com. relacionado'
        WHEN tipofac = 3012 THEN 'Devolución compra nacional'
        WHEN tipofac = 3028 THEN 'Compra peso'
        WHEN tipofac = 3029 THEN 'Suscrip. peso'
        WHEN tipofac = 3030 THEN 'Devolución compra peso'
        WHEN tipofac in ( 3009, 3031, 5) THEN 'Nacional'
        END like mastercard.nombre  AND mastercard.linea = 'mastercard' AND (es_comercio_presencial is false OR es_comercio_presencial is null ) AND DATE(m.fecha_creacion  , "America/Santiago") between mastercard.fecha_inicio and mastercard.fecha_fin
    
    LEFT JOIN {{ source('aux', 'mastercard_revenue') }} mastercard_phys on
    CASE 
        WHEN tipofac = 3006 THEN 'Suscrip. dist. peso'
        WHEN tipofac = 3007 THEN 'Compra dist. peso'
        WHEN tipofac = 3010 THEN 'Devolución compra'
        WHEN tipofac = 3011 THEN 'Devolución compra com. relacionado'
        WHEN tipofac = 3012 THEN 'Devolución compra nacional'
        WHEN tipofac = 3028 THEN 'Compra peso'
        WHEN tipofac = 3029 THEN 'Suscrip. peso'
        WHEN tipofac = 3030 THEN 'Devolución compra peso'
        WHEN tipofac in ( 3009, 3031, 5) THEN 'Nacional'
        END like mastercard_phys.nombre  AND mastercard_phys.linea = 'mastercard_physical' AND es_comercio_presencial is true AND DATE(m.fecha_creacion  , "America/Santiago") between mastercard_phys.fecha_inicio and mastercard_phys.fecha_fin
WHERE 
    tipofac in (3006,3007,3028,3029,3009, 3031, 5, 3010,3011,3012,3030) 
    AND (m.estado  in ('PROCESS_OK','AUTHORIZED') 
    OR (m.estado = 'NOTIFIED' AND DATE(m.fecha_creacion  , "America/Santiago") BETWEEN DATE_SUB(CURRENT_DATE("America/Santiago"), INTERVAL 4 DAY) AND CURRENT_DATE("America/Santiago")))
    AND indnorcor  = 0
    AND {{ dates_between('DATE(m.fecha_creacion  , "America/Santiago")') }} -- \Modelos_de_Datos\200_DBT_Tenpo_BI\dbt_tenpo_bi\macros\dates_between.sql 
QUALIFY 
    row_number() over (partition by CAST(m.uuid AS STRING) order by m.fecha_actualizacion desc) = 1