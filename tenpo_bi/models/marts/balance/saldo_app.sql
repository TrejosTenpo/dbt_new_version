{{ config(tags=["daily", "bi"], materialized='table') }}

SELECT
    DISTINCT *, CAST(NULL AS STRING) as segmento
FROM {{ ref('saldo_app_reconciliation') }}

UNION ALL

SELECT
    DISTINCT *, CAST(NULL AS STRING) as segmento
FROM {{ ref('saldo_app_users') }}

