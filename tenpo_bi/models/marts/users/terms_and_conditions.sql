
{{ 
  config(
    materialized='table', 
    tags=["daily", "bi"],
  ) 
}}

SELECT 
    *
FROM
    {{ source('tenpo_users', 'terms_and_conditions') }}