
{{ 
  config(
    materialized='table', 
    tags=["daily", "bi"],
  ) 
}}


SELECT DISTINCT
rev, 
revtstmp
         FROM
         {{ source('tenpo_users', 'revinfo') }}