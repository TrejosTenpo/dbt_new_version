
{{ 
  config(
    materialized='table', 
    tags=["daily", "bi"],
  ) 
}}

SELECT DISTINCT
notification_token, 
user_id, 
sign_in_date_time, 
device_type, 
device_state, 
appsflyer_id, 
firebase_id, 
native_device_id, 
firebase_app_instance_id
         FROM
         {{ source('tenpo_users', 'user_device') }}