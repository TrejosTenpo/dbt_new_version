{% set partitions_between_to_replace = [
    'date_sub(current_date, interval 15 day)',
    'current_date'
] %}

{{ 
  config(
    materialized='table', 
    tags=["daily", "bi"],
    partition_by = { 'field': 'date', 'data_type': 'date' },
    incremental_strategy = 'insert_overwrite'
  )
}}


SELECT 
    date(c.creacion) as date,
    c.creacion as fecha_creacion,
    c.actualizacion fecha_actualizacion,
    u.uuid as user,
    c.uuid as account_id, 
    saldo_info,
    saldo_expiracion,
    account_reconciled,
    c.cuenta as contrato,
    c.nivel,
    case when user.plan = 1 and user.category = 'C1' then 'JR' else c.tipo end as segmento,  --plan 1 = JR -- category C1 = Cliente ready
    c.tipo_validacion
FROM {{ source('prepago', 'prp_cuenta') }} c 
JOIN {{ source('prepago', 'prp_usuario') }} u on c.id_usuario = u.id 
LEFT JOIN {{ ref('users_tenpo')}} user on u.uuid = user.id
QUALIFY ROW_NUMBER () OVER (PARTITION BY c.uuid ORDER BY actualizacion DESC) = 1


{% if is_incremental() %}
    and date between {{ partitions_between_to_replace | join(' and ') }}
{% endif %}