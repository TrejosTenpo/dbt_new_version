/*
    Welcome to your first dbt model!
    Did you know that you can also configure models directly within SQL files?
    This will override configurations stated in dbt_project.yml

    Try changing "table" to "view" below
*/

{{ 
  config(
    tags=["hourly_all_day", "bi"],
    materialized='incremental', 
    cluster_by = "linea",
    partition_by = {'field': 'fecha', 'data_type': 'date'},
    incremental_strategy = 'insert_overwrite'
  ) 
}}


WITH 
  economics_app AS (
    SELECT fecha ,trx_timestamp ,nombre ,monto ,trx_id ,user ,account_id ,linea ,canal ,TRIM(comercio) comercio ,id_comercio ,actividad_cd ,actividad_nac_cd ,codact, tipofac, revenue, codpais, id_tarjeta
    FROM {{ ref('int_transform_mastercard_physical') }}
    UNION ALL
    SELECT fecha ,trx_timestamp ,nombre ,monto ,trx_id ,user ,account_id ,linea ,canal ,TRIM(comercio) comercio ,id_comercio ,actividad_cd ,actividad_nac_cd ,codact, tipofac, revenue, codpais, id_tarjeta
    FROM {{ ref('int_transform_mastercard_virtual') }}
    )
    ,economic_pre_total as
    
   (SELECT DISTINCT
     a.user
     ,a.* EXCEPT(user, codpais)
     ,concat(a.linea, '_', trx_id, '_', trx_timestamp) as economics_trx_id
     ,DATETIME(trx_timestamp,  "America/Santiago") as dt_trx_chile
     ,b.* EXCEPT(linea, comercio)
     ,cat_tenpo1
     ,cat_tenpo2
     ,cat_tenpo3
     ,d.tipo tipo_trx
     ,d.glosa
     ,case 
        when nombre="QR Payment" then "Redpay"
        when nombre="Cashin Pago Nomina" then "Payroll"
        --when nombre like "Cashout ATM%" then "Cashout ATM"
        else a.linea
      end as nueva_linea
     ,codp.pais as pais_trx
     ,mcc_master.edited_description as mcc_desc 
   FROM economics_app a
   LEFT JOIN {{ ref('rubros_comercios')}} b ON a.linea = b.linea and a.comercio = b.comercio
   LEFT JOIN {{ ref('stg_aux_table__mcc_codes')}} ON CAST(codact AS INT64) = CAST(MCC  as INT64)
   LEFT JOIN (SELECT * FROM {{ ref('stg_prepago__tipos_factura')}} WHERE indnorcor = 0) d  USING(tipofac)
   LEFT JOIN {{ ref('stg_aux_table__codigo_pais')}} codp ON a.codpais = codp.ISO_3166_1_num
   LEFT JOIN {{ ref('stg_aux_table__mcc_master_codes')}} mcc_master ON a.codact=mcc_master.mcc
   WHERE fecha <= CURRENT_DATE("America/Santiago")
   QUALIFY row_number() over (partition by concat(a.linea, '_', trx_id, '_', trx_timestamp) order by trx_timestamp desc) = 1),

   economic_total as 
   (
    SELECT 
    ec.* EXCEPT(nombre)
    ,CASE WHEN cof.id_comercio is not null then 'Suscripcion' else ec.nombre end as nombre 
    FROM economic_pre_total ec
    LEFT join {{ ref('stg_aux_table__cof_comercios')}} cof
    on cof.comercio_recod = ec.comercio_recod and cof.id_comercio = ec.id_comercio and ec.nombre not in ('Devolución compra','Devolución compra peso') and ec.linea IN ('mastercard', 'credit_card_physical', 'credit_card_virtual')
   )

   ,economics_users as

   (SELECT DISTINCT  ec.user,{{ hash_sensible_data('u.email') }} as email,ec.* EXCEPT(user) FROM economic_total ec
    JOIN {{ ref('users_tenpo')}} u ON u.id = ec.user
   UNION ALL 
   SELECT DISTINCT u.business_id, '' as email,ec.* EXCEPT(user) FROM economic_total ec
    JOIN {{ ref('business')}} u ON u.business_id = ec.user)   

  , economics as 
  (SELECT DISTINCT * FROM
    (SELECT ec.* EXCEPT(account_id),  accounts.segmento, ec.account_id as id_cuenta FROM (SELECT * FROM economics_users 
              WHERE linea NOT IN ('cashin','cashout','p2p_enviado','p2p_recibido','mastercard_physical','mastercard_virtual','rewards')) ec 
    JOIN (SELECT * FROM {{ ref('accounts')}} WHERE segmento IN ('PERSONAL','JR')) accounts ON accounts.user = ec.user

    UNION ALL

    SELECT ec.* EXCEPT(account_id),  accounts.segmento, ec.account_id as id_cuenta FROM (SELECT * FROM economics_users 
              WHERE linea IN ('cash_in','cash_out','p2p','p2p_received','mastercard','mastercard_physical','reward')) ec 
    JOIN (SELECT * FROM {{ ref('accounts')}} WHERE segmento IN ('PERSONAL','JR','FREELANCE','BUSINESS')) accounts ON accounts.user = ec.user AND accounts.account_id = ec.account_id)
  QUALIFY row_number() over (partition by concat(linea, '_', trx_id, '_', trx_timestamp) order by trx_timestamp desc ,segmento asc) = 1
  )

  , add_user_limit_card_tc AS (
    select t1.*, 
    CASE 
    WHEN t2.limit_card < 500000 THEN "<500.000"
    WHEN t2.limit_card BETWEEN 500000 AND 1500000 THEN "500.000 a 1.500.000"
    WHEN t2.limit_card > 1500000 THEN ">1.500.000"
    END as limit_card_category
    from economics AS t1
    left join {{ ref('int_transform_user_product_tc')}} AS t2 ON t1.user = t2.user_id
  )

-- Agregamos la categoria 3 de tenpo actualizada. (pedida de vicho 2023-10-11) (ver como mejorar este proceso.)
, add_cat_tenpo3_commerce as (
SELECT  
  ec.user,
  ec.email,
  ec.fecha,
  ec.trx_timestamp,
  ec.monto,
  ec.trx_id,
  ec.linea,
  ec.canal,
  ec.comercio,
  ec.id_comercio,
  ec.actividad_cd,
  ec.actividad_nac_cd,
  ec.codact,
  ec.tipofac,
  ec.revenue,
  ec.id_tarjeta,
  ec.economics_trx_id,
  ec.dt_trx_chile,
  ec.comercio_recod,
  ec.rubro_recod,
  ec.cat_tenpo1,
  ec.cat_tenpo2,
  CASE WHEN ct3.category_new IS NOT NULL THEN ct3.category_new ELSE ec.cat_tenpo3 END AS cat_tenpo3,
  ec.tipo_trx,
  ec.glosa,
  ec.nueva_linea,
  ec.pais_trx,
  ec.mcc_desc,
  ec.nombre,
  ec.segmento,
  ec.id_cuenta,
  ec.limit_card_category
FROM add_user_limit_card_tc AS ec
LEFT JOIN {{ ref('stg_aux_table__cat_tenpo3_commerce_mat')}} AS ct3
ON ec.comercio = ct3.nombre_comercio
),


add_cat_tenpo3_comercio_recod as (
SELECT 
  ec.user,
  ec.email,
  ec.fecha,
  ec.trx_timestamp,
  ec.monto,
  ec.trx_id,
  ec.linea,
  ec.canal,
  ec.comercio,
  ec.id_comercio,
  ec.actividad_cd,
  ec.actividad_nac_cd,
  ec.codact,
  ec.tipofac,
  ec.revenue,
  ec.id_tarjeta,
  ec.economics_trx_id,
  ec.dt_trx_chile,
  ec.comercio_recod,
  ec.rubro_recod,
  ec.cat_tenpo1,
  ec.cat_tenpo2,
  CASE WHEN ct3cr.cat_tenpo3 IS NOT NULL THEN ct3cr.cat_tenpo3 ELSE ec.cat_tenpo3 END AS cat_tenpo3,
  ec.tipo_trx,
  ec.glosa,
  ec.nueva_linea,
  ec.pais_trx,
  ec.mcc_desc,
  ec.nombre,
  ec.segmento,
  ec.id_cuenta,
  ec.limit_card_category
FROM add_cat_tenpo3_commerce AS ec
LEFT JOIN {{ ref('stg_aux_table__cat_tenpo3_comercio_recod_mat')}} as ct3cr
ON ec.comercio_recod = ct3cr.comercio_recod
)

SELECT
  user,
  email,
  fecha,
  trx_timestamp,
  monto,
  trx_id,
  linea,
  canal,
  comercio,
  id_comercio,
  actividad_cd,
  actividad_nac_cd,
  codact,
  tipofac,
  revenue,
  id_tarjeta,
  economics_trx_id,
  dt_trx_chile,
  comercio_recod,
  rubro_recod,
  cat_tenpo1,
  cat_tenpo2,
  cat_tenpo3,
  tipo_trx,
  glosa,
  nueva_linea,
  pais_trx,
  mcc_desc,
  nombre,
  segmento,
  id_cuenta,
  limit_card_category
FROM
  add_cat_tenpo3_comercio_recod
