{% macro dates_between(variable) %}


IF(format_date('%A', DATE(current_date())) = 'Saturday' AND extract(HOUR FROM current_timestamp()) = 20,
    DATE({{ variable }}) BETWEEN date_sub(current_date(), interval 20 YEAR) AND current_date(),
    DATE({{ variable }}) BETWEEN date_sub(current_date(), interval 1 DAY) AND current_date())


{% endmacro %}